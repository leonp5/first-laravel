<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');
Route::get('/info', 'PagesController@info');
Route::get('/privacy', 'PagesController@privacy');
Route::get('/disclaimer', 'PagesController@disclaimer');
Route::get('/success', 'PagesController@success');

Route::resource("posts", "PostsController");

Auth::routes();

