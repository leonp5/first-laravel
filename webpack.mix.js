const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js").sass(
    "resources/sass/app.scss",
    "public/css"
);

mix.copyDirectory("resources/images", "public/images")
    .copyDirectory("resources/fonts", "public/fonts")
    .copyDirectory("resources/js/components", "public/js/components");

mix.browserSync({
    proxy: "http://127.0.0.1:8000",
    files: [
        "resources/views/*.php",
        "resources/views/pages/*.php",
        "resources/views/database/*.php",
        "resources/views/components/*.php",
        "routes/*.php",
        "resources/js/components/*.js",
        "app/Http/*.php",
        "app/Http/Controllers/*.php"
    ],
    // => true = the browse opens a new browser window with every npm run watch startup/reload

    open: false,
    notify: false
});
