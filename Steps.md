## Schritte/Addons

-   Browsersync installiert und konfiguriert

-   Blade SVG installiert:

    -   [Medium](https://medium.com/@simondepelchin/how-to-use-font-awesome-5-svgs-with-laravel-blade-a5dc93743cd0)

    -   [GitHub](https://github.com/adamwathan/blade-svg)

-   [Laravel Collective HTML](https://laravelcollective.com/docs) Package installiert
