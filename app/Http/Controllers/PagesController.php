<?php

namespace App\Http\Controllers;


class PagesController extends Controller
{
    public function index(){
        
        return view('pages.index');
    }

    public function about(){
        
        return view('pages.about');
    }

    public function contact(){
        
        return view('pages.contact');
    }

    public function info(){
        
        return view('pages.info');
    }
    public function privacy(){
        
        return view('pages.privacy');
    }
    public function disclaimer(){
        
        return view('pages.disclaimer');
    }

    public function success(){
        
        return view('pages.success');
    }



}