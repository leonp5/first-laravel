@extends ("layouts.app")

@section('slider')

@include('components.slider')

@endsection

@section ("content")


<h1 class="align-left">Lorem, ipsum.</h1>
<div class="content-row">
  <div class="content-column">
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem unde laborum id tempora veniam,
      molestias eius magni, saepe nemo rerum placeat laudantium consectetur velit cupiditate deserunt quae? Repudiandae
      soluta aliquid minima reprehenderit quam unde eum similique maxime hic doloremque recusandae deserunt suscipit,
      maiores inventore natus quae ipsum distinctio enim fuga temporibus eligendi ipsam! Vel itaque quia asperiores!
      Nesciunt cum accusamus sequi. Illum quidem voluptatem doloremque, hic temporibus laborum velit distinctio ducimus,
      vero ipsam quibusdam, id praesentium voluptate reprehenderit. Eum incidunt nulla facere a, earum nihil quibusdam,
      adipisci repudiandae cumque omnis modi quidem tenetur corporis fugit quas numquam. Soluta, fuga debitis!</p>
  </div>
  <div class="gap"></div>
  <div class="content-column">
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Amet possimus iure soluta
      assumenda fugit similique sint, perferendis illo officiis quod, culpa pariatur, qui suscipit? Quasi vitae
      consequuntur labore, quia laboriosam dicta error necessitatibus reiciendis ipsam accusamus, assumenda illo ipsa
      cum
      a sint reprehenderit fuga recusandae, quam culpa sit velit! Atque, assumenda corrupti voluptatibus, ut quae
      temporibus architecto, exercitationem asperiores ad dolorum maxime dolorem eveniet? Rem, voluptate mollitia
      doloremque aperiam repellendus nulla voluptas architecto veniam inventore maxime assumenda quo, laboriosam odit,
      quibusdam dolores? Minima quam aliquam voluptate, nulla debitis, aut vero dolores, rerum voluptatem placeat
      laudantium facilis non corrupti quaerat expedita.</p>
  </div>
</div>
<div class="teaser-row">
  <a class="teaser-link" href="/about">
    <div class="teaser-card">
      @svg("about", "teaser-logo")
      <h4 class="teaser-heading">Über</h4>
      <div class="text-wrapper">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque illum quod porro minima
          nisi
          eos esse. Accusamus iste excepturi neque blanditiis iusto harum dolorem officia, obcaecati aperiam quis,
          soluta
          corrupti commodi cumque repudiandae itaque vero rem illo eius assumenda nulla quasi omnis dolor. Ea nulla cum
          unde optio odit eius?.</p>
      </div>
    </div>
  </a>
  <a class="teaser-link" href="/info">
    <div class="teaser-card">
      @svg("info", "teaser-logo")
      <h4 class="teaser-heading">Info</h4>
      <div class="text-wrapper">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque illum quod porro minima
          nisi
          eos esse. Accusamus iste excepturi neque blanditiis iusto harum dolorem officia, obcaecati aperiam quis,
          soluta
          corrupti commodi cumque repudiandae itaque vero rem illo eius assumenda nulla quasi omnis dolor. Ea nulla cum
          unde optio odit eius?.</p>
      </div>
    </div>
  </a>
  <a class="teaser-link" href="/contact">
    <div class="teaser-card">
      @svg("contact", "teaser-logo")
      <h4 class="teaser-heading">Kontakt</h4>
      <div class="text-wrapper">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque illum quod porro minima
          nisi
          eos esse. Accusamus iste excepturi neque blanditiis iusto harum dolorem officia, obcaecati aperiam quis,
          soluta
          corrupti commodi cumque repudiandae itaque vero rem illo eius assumenda nulla quasi omnis dolor. Ea nulla cum
          unde optio odit eius?.</p>
      </div>
    </div>
  </a>
</div>
<div class="bottom-container">
  <div class="right-image-wrapper">
    <img class="right-image" src="/images/jump_preview.jpg" alt="jump in the sunset" />
    <a class="img-modal-link" data-toggle="modal" data-target="#imageModal">
      <p class="image-underline">Vergrößern</p>
    </a>
    @include("components.imageModal")
  </div>
  <p>
    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab porro iusto iure sapiente dolores
    labore ullam nemo nobis nulla in doloremque rem praesentium molestias deleniti vero autem totam, perferendis,
    commodi
    aliquid eveniet quasi eaque? Quas velit quos ullam porro. Perspiciatis iste corrupti autem nisi numquam blanditiis
    quo
    quod amet a reiciendis laborum corporis tempore accusamus dicta labore facilis placeat tempora delectus vel, magnam
    dolorum minus laudantium dolores! Expedita eaque, reiciendis neque molestias animi, sed similique officia
    consequatur
    officiis natus impedit odio sapiente quos? Error perspiciatis vel dignissimos recusandae animi quidem neque.
    Voluptatem ad, laboriosam tenetur maiores ipsum eos molestiae! Illum voluptates quod magni repudiandae ipsam dolorem
    explicabo, aspernatur, quibusdam nesciunt expedita voluptate sapiente voluptatum recusandae et! Commodi inventore
    fuga
    asperiores mollitia nihil numquam veritatis temporibus corporis, nulla delectus necessitatibus. Ea officia doloribus
    maiores accusantium quas? Omnis laudantium praesentium, repellendus exercitationem iste placeat ipsum quo quia,
    architecto officiis eaque optio assumenda quidem cum ipsa? Nesciunt cupiditate voluptates praesentium error! Numquam
    soluta placeat eum eligendi et commodi, esse cupiditate nisi minima vel cumque cum delectus, sunt consequuntur vero
    facilis explicabo enim officia autem dolore harum. Delectus consectetur quas omnis rerum in tenetur magni facere,
    ratione voluptatum amet eligendi, laboriosam similique unde sapiente!
  </p>
</div>
@endsection