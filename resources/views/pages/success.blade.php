@extends('layouts.app')

@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
  {{ session('status') }}
</div>
@endif

<h4>Du bist eingeloggt!</h4>
<a class="inline-link" href="/posts">Hier gehts zum "Blog"</a>

@endsection