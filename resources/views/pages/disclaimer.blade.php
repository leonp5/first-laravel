@extends ("layouts.app")

@section ("content")

<h1 class="margin-bottom">Impressum</h1>

<div class="content-row">

  <div class="content-column">
    <h4>Fotos</h4>
    <p>Fotos von <a class="inline-link" href="https://www.pexels.com/de-de/@oidonnyboy" target="_blank"
        rel="noopener">Nick
        Wehrli</a>, <a class="inline-link" href="https://www.pexels.com/de-de/@kateryna-babaieva-1423213"
        target="_blank" rel="noopener">Kateryna
        Babaieva</a>, <a class="inline-link" href="https://www.pexels.com/de-de/@trushotz-2012836" target="_blank"
        rel="noopener">TruShotz</a> und <a class="inline-link"
        href="https://www.pexels.com/de-de/@sidney-recato-2084924" target="_blank" rel="noopener">Sidney Recato</a>.
    </p>
  </div>

  <div class="gap"></div>

  <div class="content-column">
    <h4>Icons</h4>
    <p>
      Icons made by <a class="inline-link" href="https://www.flaticon.com/authors/freepik" target="_blank"
        rel="noopener">Freepik</a>, <a class="inline-link" href="https://www.flaticon.com/authors/smashicons"
        target="_blank" rel="noopener">Smashicons</a>, <a class="inline-link"
        href="https://www.flaticon.com/authors/srip" rel="noopener" target="_blank">Srip</a> and <a class="inline-link"
        href="https://www.flaticon.com/authors/those-icons" target="_blank" rel="noopener">Those Icons</a> from <a
        class="inline-link" href="https://www.flaticon.com/" target="_blank" rel="noopener">www.flaticon.com</a>.
      <br /> <br />
      Laravel Icon: <a class="inline-link" href="https://de.wikipedia.org/wiki/Laravel#/media/Datei:Laravel.svg"
        target="_blank" rel="noopener">Wikipedia</a> </p>
  </div>
</div>
<div class="content-row">
  <div class="content-column">
    <h4>Angaben gemäß § 5 TMG</h4>
    <p>
      Leon Pelzer <br />
      Hahnenweg 1 <br />
      51061 Köln
    </p>
  </div>
  <div class="gap"></div>
  <div class="content-column">
    <h4>Kontakt</h4>
    <p>
      E-Mail:
      <a class="inline-link" href="mailto:leonpe@web.de">leonpe@web.de</a>
    </p>
  </div>
</div>
<div class="bottom-container">
  <h4>Haftung für Inhalte</h4>
  <p>
    Als Diensteanbieter bin ich gemäß § 7 Abs.1 TMG für eigene Inhalte auf
    diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8
    bis 10 TMG bin ich als Diensteanbieter jedoch nicht verpflichtet,
    übermittelte oder gespeicherte fremde Informationen zu überwachen oder
    nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit
    hinweisen. <br /> <br />
    Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
    Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt.
    Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
    Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
    von entsprechenden Rechtsverletzungen werde ich diese Inhalte umgehend
    entfernen.
  </p>
  <h4>Haftung für Links</h4>
  <p>
    Mein Angebot enthält Links zu externen Websites Dritter, auf deren
    Inhalte ich keinen Einfluss habe. Deshalb kann ich für diese fremden
    Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten
    Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten
    verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der
    Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige
    Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. <br />
    <br />
    Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch
    ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
    Bekanntwerden von Rechtsverletzungen werde ich derartige Links
    umgehend entfernen.
  </p>
  <h4>Urheberrecht</h4>
  <p>
    Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt
    wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden
    Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf
    eine Urheberrechtsverletzung aufmerksam werden, bitte ich um einen
    entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen
    werde ich derartige Inhalte umgehend entfernen. <br /> <br />
    Quelle:
    <a class="inline-link" href="https://www.e-recht24.de/impressum-generator.html" target="_blank" rel="noopener">
      e-recht24.de
    </a>
  </p>

</div>
@endsection