@extends ("layouts.app")

@section ("content")
<h1>Kontakt</h1>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta enim corrupti suscipit quam! Pariatur
  neque aperiam quasi harum possimus, expedita illo optio deleniti, recusandae incidunt consectetur blanditiis unde sit
  soluta iste nemo libero voluptatum officia fugiat ex similique tempore a nihil iusto. Sed, unde ipsam modi ex
  dignissimos soluta corporis nesciunt quo voluptas nam voluptate eum qui excepturi consequuntur asperiores magnam atque
  repudiandae et repellendus ipsa non error, omnis temporibus illum. Sequi maxime fuga reiciendis, sunt commodi
  obcaecati omnis aliquid nostrum officiis ea repudiandae, iure, similique rem iste ducimus molestiae. Doloremque animi,
  placeat quasi qui accusamus dignissimos pariatur rerum eaque fugit natus repudiandae culpa eos, saepe voluptatem
  maxime numquam necessitatibus consequatur consectetur asperiores. Quisquam totam aspernatur rerum expedita repellat
  consequatur, cupiditate voluptatibus similique ducimus, harum earum provident non possimus aut a minima. </p>

@include("components.contactMap")

@endsection