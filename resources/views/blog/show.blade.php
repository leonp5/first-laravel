@extends ("layouts.app")

@section ("content")
<a class="back-button" href="/posts">
  <p class="back-arrow">&larr;</p> Zur Übersicht
</a>

<div class="post-container">
  <h2>{{$post->title}}</h2>
  <p class="small">Erstellt am {{$post->created_at->format('d.m.Y')}}</p>
  <p class="post-text">{{$post->body}}</p>
</div>
@if(!Auth::guest())
<div class="form-bottom">

  {!!Form::open(["action" => ["PostsController@destroy", $post->id], "method" => "POST"])!!}
  {{Form::hidden("_method", "DELETE")}}
  {{Form::submit("Löschen", ["class" => "delete-button"])}}
  {!!Form::close()!!}
  <a class="edit-button" href="/posts/{{$post->id}}/edit">Bearbeiten</a>
</div>
@endif
@endsection