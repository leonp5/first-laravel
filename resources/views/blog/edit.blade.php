@extends ("layouts.app")

@section ("content")
<a class="back-button" href="/posts/{{$post->id}}">
  <p class="back-arrow">&larr;</p> Zurück zum Eintrag
</a>
{!! Form::open(["action" => ["PostsController@update", $post->id], "method" => "POST"]) !!}
<h2 class="align-left">Eintrag bearbeiten...</h2>
<div class="form-group">
  {{Form::label("title", "Titel")}}
  {{Form::text("title", $post->title, ["class" => "form-control", "placeholder" => "Titel"])}}
</div>
<div class="form-group">
  {{Form::label("body", "Text")}}
  {{Form::textarea("body", $post->body, ["class" => "form-control", "placeholder" => "Text..."])}}
</div>
{{Form::hidden("_method", "PUT")}}
<div class="form-bottom">
  <a class="abort-button" href="/posts">Abbrechen</a>
  {{Form::submit("Speichern", ["class" => "basic-button"])}}
</div>
{!! Form::close() !!}


@endsection