@extends ("layouts.app")

@section ("content")
<a class="back-button" href="/posts">
  <p class="back-arrow">&larr;</p> Zur Übersicht
</a>
{!! Form::open(["action" => "PostsController@store", "method" => "POST"]) !!}
<h2 class="align-left">Neuer Eintrag</h2>
<div class="form-container">
  <div class="form-group">
    {{Form::label("title", "Titel")}}
    {{Form::text("title", "", ["class" => "form-control", "placeholder" => "Titel"])}}
  </div>
  <div class="form-group">
    {{Form::label("body", "Text")}}
    {{Form::textarea("body", "", ["class" => "form-control", "placeholder" => "Text..."])}}
  </div>
  <div class="form-bottom">
    <a class="abort-button" href="/posts">Abbrechen</a>
    {{Form::submit("Eintragen", ["class" => "basic-button"])}}
  </div>
</div>
{!! Form::close() !!}


@endsection