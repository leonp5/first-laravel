@extends ("layouts.app")

@section ("content")

<h1>Einträge</h1>
@if(!Auth::guest())
<div class="add-wrapper">
  @svg("plus", "add" )
  <a class="inline-link" href="/posts/create"> Neuen Eintrag erstellen</a>
  <a class="inline-link" href="/posts/create">
</div>
@endif
@if (count($posts) > 0 )
@foreach ($posts as $post)
<div class="post-container">
  <h4><a class="inline-link" href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
  <p>Erstellt am {{$post->created_at->format('d.m.Y')}} um {{$post->created_at->format("H:i")}} Uhr.</p>
</div>
@endforeach

@else

<p>Keine Einträge gefunden.</p>

@endif

@endsection