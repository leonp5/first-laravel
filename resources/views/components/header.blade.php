<div class="header">
  <div class="header-wrapper">
    <a class="logo-link" href="/">

      @svg("logo", "logo")

    </a>
    <div class="Navbar">
      @include('components.navigation')
    </div>
    <input type="checkbox" class="mobile-menu-toggle" />
    <div class="hamburger">
      <div></div>
    </div>
    <div class="mobile-menu">
      <div class="menu-animater">
        @include('components.navigation')
      </div>
    </div>
  </div>
</div>