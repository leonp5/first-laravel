<div class="modal" id="imageModal" tabindex="-1" role="dialog">
  <div class="img-modal-background">
    <input type="checkbox" class="close-button" data-dismiss="modal"/> <a class="close-cross"></a>
    <img class="modal-image" src="/images/jump.jpg" alt="jump in the sunset" />
  </div>
</div>