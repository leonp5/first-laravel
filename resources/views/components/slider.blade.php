<div class="slider-wrapper">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ URL::to('/images/rocks.jpg') }}" class="slider-image" alt="rocks at the sea">
      </div>
      <div class="carousel-item">
        <img src="{{ URL::to('/images/road.jpg') }}" class="slider-image" alt="road threw mountains">
      </div>
      <div class="carousel-item">
        <img src="{{ URL::to('/images/oven.jpg') }}" class="slider-image" alt="man on a oven">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>