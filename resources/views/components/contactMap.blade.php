@section('head')

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin="" />

@endsection

<div onWheel="enableListeners()" onTouchMove="enableListeners()" onMouseOut="disableListeners()"
  onTouchEnd="disableListeners()" class="contact-map" id="map"></div>

@push('scripts')

<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>

<script src="{{ asset('js/components/ContactMap.js')}}"></script>
<script src="{{ asset('js/components/MapFunctionality.js')}}"></script>

@endpush