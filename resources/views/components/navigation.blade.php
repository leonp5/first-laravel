<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<a class="{{Request::path() === "/" ? "NavbarLinkActive" : "NavbarLink" }}" href="/">Home</a>
<a class="{{Request::path() === "about" ? "NavbarLinkActive" : "NavbarLink" }}" href="/about">Über</a>
<a class="{{Request::path() === "info" ? "NavbarLinkActive" : "NavbarLink" }}" href="/info">Info</a>
<a class="{{Request::path() === "contact" ? "NavbarLinkActive" : "NavbarLink" }}" href="/contact">Kontakt</a>
<a class="{{Request::path() === "posts" ? "NavbarLinkActive" : "NavbarLink" }}" href="/posts">Blog</a>

@guest
<a class="{{Request::path() === "login" ? "NavbarLinkActive" : "NavbarLink" }}"
  href="{{ route('login') }}">{{ __('Login') }}</a>
@if (Route::has('register'))
<a class="{{Request::path() === "register" ? "NavbarLinkActive" : "NavbarLink" }}"
  href="{{ route('register') }}">{{ __('Registrieren') }}</a>
@endif
@else

<a class="NavbarLink" href="{{ route('logout') }}" onclick="event.preventDefault();
 document.getElementById('logout-form').submit();">
  {{ __('Ausloggen') }}</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
@endguest