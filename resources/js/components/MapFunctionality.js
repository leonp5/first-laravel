function enableListeners() {
    document.addEventListener("wheel", showZoomHint);
    document.addEventListener("dblclick", showZoomHint);
    document.addEventListener("keydown", enableZoom);
    document.addEventListener("touchmove", showDragHint);
}

function disableListeners() {
    document.removeEventListener("wheel", showZoomHint);
    document.removeEventListener("dblclick", showZoomHint);
    document.removeEventListener("touchend", removeDragHint);
    document.removeEventListener("touchcancel", removeDragHint);
    document.removeEventListener("touchmove", removeDragHint);
    document.getElementById("map").classList.remove("zoomHint");
    document.getElementById("map").classList.remove("dragHint");
}

function showZoomHint(e) {
    function checkUserAgent() {
        if (navigator.platform.toUpperCase().indexOf("MAC") >= 0) {
            return true;
        } else {
            return false;
        }
    }

    const MAC = checkUserAgent();

    if (e.type === ("wheel" || "dblclick") && MAC === true) {
        document.getElementById("map").classList.add("macZoomHint");
        setTimeout(() => {
            document.getElementById("map").classList.remove("macZoomHint");
        }, 3000);
    } else {
        document.getElementById("map").classList.add("zoomHint");
        setTimeout(() => {
            document.getElementById("map").classList.remove("zoomHint");
        }, 3000);
    }
}

function showDragHint(e) {
    if (e.type === "touchmove" && e.touches.length === 1) {
        document.getElementById("map").classList.add("dragHint");
        document.addEventListener("touchend", removeDragHint);
        document.addEventListener("touchcancel", removeDragHint);
    }
    document.removeEventListener("touchmove", showDragHint);
}

function removeDragHint(e) {
    if (e.type === "touchend" || "touchcancel") {
        document.getElementById("map").classList.remove("dragHint");
        document.removeEventListener("touchend", removeDragHint);
        document.removeEventListener("touchcancel", removeDragHint);
        document.removeEventListener("touchmove", removeDragHint);
    }
}

function enableZoom(e) {
    if (e.ctrlKey || e.metaKey) {
        document.getElementById("map").classList.remove("zoomHint");
        map.scrollWheelZoom.enable();
        map.doubleClickZoom.enable();
        document.addEventListener("keyup", disableZoom);
    }
    if (e.type === "touchmove" && e.touches.length === 2) {
        document.getElementById("map").classList.remove("dragHint");
        map.dragging.enable();
        document.addEventListener("touchend", disableZoom);
    }
}

function disableZoom(e) {
    if (e.type === "keyup") {
        map.scrollWheelZoom.disable();
        map.doubleClickZoom.disable();
        document.removeEventListener("keyup", disableZoom);
    }
    if (e.type === "touchend") {
        map.dragging.disable();
        document.removeEventListener("touchend", disableZoom);
    }
}
