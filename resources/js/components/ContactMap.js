function checkUserAgent() {
    if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
        )
    ) {
        return false;
    } else {
        return true;
    }
}

const pc = checkUserAgent();

const position = [50.9775583, 7.0715246];

const map = L.map("map", {
    scrollWheelZoom: false,
    doubleClickZoom: false,
    dragging: pc
}).setView(position, 17);

const marker = L.marker(position).addTo(map);

marker.bindPopup("<b>Adresse:</b><br> Dellbrücker Hauptstraße 137, 51069 Köln");

L.tileLayer("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png", {
    attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
